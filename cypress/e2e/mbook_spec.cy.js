describe('mbook specs', () => {
  var timer = 1500

  it('display books list', () => {
    cy.visit('/')
    cy.wait(timer)
    cy.get(".list-group-item").click({multiple: true})
    cy.wait(timer)
    cy.get('#page-title').should('have.text', 'Books List')
    cy.wait(timer)
  })

  it('add book successfully', () => {
    cy.visit("/add")
    cy.wait(timer)
    cy.get("#title").type("R for beginners")
    cy.wait(timer)
    cy.get("#author").type("Christian Ndaya")
    cy.wait(timer)
    cy.get("#description").type("A complete guide on R language for beginners")
    cy.wait(timer)
    cy.get("#submit").click()
    cy.wait(timer)
    cy.get("#success-msg").should('be.visible')
    cy.wait(timer)
    //cy.get("#add-btn").click()
  })

  it('update book successfully', () => {
    cy.visit("/books")
    cy.wait(timer)
    cy.get("ul").contains("R for beginners").click()
    cy.wait(timer)
    cy.get("#edit-btn").click()
    cy.wait(timer)
    cy.get("#author").clear()
    cy.wait(timer)
    cy.get("#author").type("Armand Meppa")
    cy.wait(timer)
    cy.get("#update-btn").click()
    cy.wait(timer)
    cy.get("#update-success-msg").should('be.visible')
    cy.wait(timer)
  })

  it('delete book successfully', () => {
    cy.visit("/books")
    cy.wait(timer)
    cy.get("ul").contains("R for beginners").click()
    cy.wait(timer)
    cy.get("#edit-btn").click()
    cy.wait(timer)
    cy.get("#delete-btn").click()
    cy.wait(timer)
    cy.get('#page-title').should('have.text', 'Books List')
  })

})
